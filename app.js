const express = require('express');
const path = require('path');
const bcrypt = require('bcrypt');
const router = require('./routes');
const { user } = require('./models');
const cors = require("cors");
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var session = require('express-session');
var flash = require('connect-flash');
const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded());
app.set("views", path.join(__dirname, "views"));
app.use(express.static("views/styles"));
app.set("view engine", "ejs");


app.use(
  session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true,
  })
);
app.use(flash());
// passport config
passport.use(
  'local',
  new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
    },
    (email, password, done) => {
      console.log('masuk passport');
      user
        .findOne({
          where: {
            email: email,
          },
          raw: true,
        })
        .then((parent) => {
          console.log(parent);
          if (parent) {
            const encryptedPassword = parent.password;

            if (bcrypt.compareSync(password, encryptedPassword)) {
              return done(null, parent);
            } else {
              return done(null, false);
            }
          } else {
            return done(null, false);
          }
        });
    }
  )
);
passport.use(
  'userlocal',
  new LocalStrategy(
    {
      usernameField: 'username',
      passwordField: 'password',
    },
    (username, password, done) => {
      console.log('masuk passport');
      user
        .findOne({
          where: {
            username: username,
          },
          raw: true,
        })
        .then((child) => {
          console.log(child);
          if (child) {
            const encryptedPassword = child.password;

            if (bcrypt.compareSync(password, encryptedPassword)) {
              return done(null, child);
            } else {
              return done(null, false);
            }
          } else {
            return done(null, false);
          }
        });
    }
  )
);

passport.serializeUser((user, done) => {
  done(null, user);
});
passport.deserializeUser((obj, done) => {
  done(null, obj);
});

//passport middleware
app.use(passport.initialize());
app.use(passport.session());

app.use('/', router);
app.use('/static', express.static('./views')); //untuk load path yang diperlukan di halaman

app.use((err, req, res, next) => {
  if (err) {
    console.log(err);
  }
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

module.exports = app;
