
module.exports = {
     success(res, data) {
        res.json({
           'status': 200,
           'message': 'Success',
           'data': data
       })
   },
   
    notFound(res){
        res.json({
           'status': 400,
           'message': 'Data tidak ditemukan',
       })
   },
    failed(res,error) {
        res.json({
           'status': 500,
           'message': 'Kesalahan pada server karena ' + JSON.stringify(error, null, 2)
       })
   }
}

