const { user, role, family, Article } = require('../models');
const bcrypt = require('bcrypt');
var passport = require('passport');
const { render } = require('ejs');

class Controller {
  static register(req, res) {
    console.log(req.body);
    const email = req.body.email;
    const password = bcrypt.hashSync(req.body.password, 10);
    const roleId = 1;
    const name = req.body.name;
    const familyname = req.body.familyname;
    var familyId = 0;

    family
      .create({
        familyname: familyname,
      })
      .then((fam) => {
        familyId = fam.id;

        user
          .create({
            email: email,
            password: password,
            roleId: roleId,
            name: name,
            familyId: familyId,
          })
          .then((ot) => {
            res.render("login", { message: req.flash("auth") });
          })
          .catch((err) => {
            console.log(err);
            // if(req.body.email == params.email){
            req.flash("authemail", "email already exist");
            res.render("register", { message: req.flash("authemail") });
          });
      });
  }

  static addmember(req, res) {
    // console.log(req.body);
    const username = req.body.username;
    const name = req.body.name;
    const password = bcrypt.hashSync(req.body.password, 10);
    const roleId = 2;
    const familyId = req.user.familyId;

    user
      .create({
        username: username,
        name: name,
        password: password,
        roleId: roleId,
        familyId: familyId,
      })
      .then((ot) => {
        // res.render("familyAddMember", {user: req.user.name});
        console.log("Berhasil menambah anak");
      })
      .catch((err) => {
        res.send(`Gagal menambahkan member anak, karena 
            ${JSON.stringify(err.message, null, 2)}`);
      });
  }

  static getMemberChild(req, res) {
    user
      .findAll({
        where: {
          familyId: req.user.familyId,
        },
      })
      .then((fam) => {
        res.json({
          status: 200,
          message: "berhasil get data child",
          data: fam,
        });
      });
  }

  static login(req, res) {
    console.log("login controller");
    console.log(req.body);
    // sess = req.session;
    // sess.email = req.body.email;

    passport.authenticate("local", {
      successRedirect: "/family",
      failureRedirect: "/login",
      failureFlash: { type: "auth", message: "Email atau Password salah." },
    })(req, res);
  }
  static loginmember(req, res) {
    console.log("login controller");
    console.log(req.body);
    // sess = req.session;
    // sess.email = req.body.email;

    passport.authenticate("userlocal", {
      successRedirect: "/family",
      failureRedirect: "/memberlogin",
      failureFlash: { type: "auth", message: "Username atau Password salah." },
    })(req, res);
  }
  static getDashboardPage(req, res) {
    res.render("family", {user: req.user});
  }

  static logout(req, res) {
    req.logout();
    res.redirect("/");
  }

  static getIndexPage(req, res) {
    if (req.user) {
      res.render("family", {user: req.user.name});
    } else {
      res.render("index");
    }
  }
  static getRegisterPage(req, res) {
    if(req.user){
      res.render("family", {user: req.user.name});
    }
    else{
      res.render("register", { message: req.flash("authemail") });
    }
    
  }
  static getLoginPage(req, res) {
    if (req.user) {
      res.render("family", {user: req.user});
    } else {
      res.render("login", { message: req.flash("auth") });
    }
  }
  static getMemberLoginPage(req, res) {
    if (req.user) {
      res.render("family", {user: req.user});
    } else {
      res.render("loginmember", { message: req.flash("auth") });
    }
  }

  static getaddMemberPage(req, res) {
    res.render("familyAddMember", {user: req.user});
  }
  static getEditMemberPage(req, res) {
    res.render("familyEditMember", {user: req.user});
  }
  static getDetailMemberPage(req, res) {
    res.render("familyDetailMember", {user: req.user});
  }
  static getAddFamilyMemberPage(req, res) {
    res.render("familyAddMember", {user: req.user});
  }
}
module.exports = Controller;
