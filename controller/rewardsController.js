const { query } = require("express");

module.exports = {
  getRewardPage: (req, res) => {
    if (req.user.roleId == 1) {
      res.render("rewards", {user: req.user});
    }else{
      res.render("rewardChild", {user: req.user});
    }
  }
};
