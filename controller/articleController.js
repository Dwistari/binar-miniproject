const { Article } = require('../models')
const sequelize = require('sequelize');

module.exports = {
    index: (req, res) => {
        Article.findAll({})
            .then(response => {
                if (response !== 0) {
                    res.json({
                        'status': 200,
                        'message': 'SUCCEED',
                        'data': response
                    })
                } else {
                    res.json({
                        'status': 400,
                        'message': 'NOT_FOUND',
                        'data': response
                    })
                }
            })
            .catch(err => {
                res.json({
                    'status': 500,
                    'message': 'ERROR'
                })
            })
    },

    get: (req, res) => {
        const articleId = req.params.id
        Article.findOne({
            where: {
                id: articleId
            }
        })
            .then(response => {
                res.json({
                    'status': 200,
                    'message': 'SUCCEED',
                    'data': response
                })
            })
            .catch(err => {
                res.json({
                    'status': 500,
                    'message': 'ERROR'
                })
            })
    },

    create: (req, res) => {
        const { title, description, image_url, slug, source } = req.body;
        console.log(req.body);
        Article.create({
            title,
            description,
            image_url,
            slug,
            source
        })
            .then(article => {
                res.json({
                    'status': 200,
                    'message': 'SUCCEED',
                    'data': article
                })
            })
            .catch(err => {
                res.json({
                    'status': 500,
                    'message': 'ERROR'
                })
            })
    },

    update: (req, res) => {
        const query = {
            where: { id: req.params.id }
        }

        const { title, description, image_url, slug, source } = req.body;
        Article.update({
            title,
            description,
            image_url,
            slug,
            source
        }, query)
            .then(article => {
                res.json({
                    'status': 200,
                    'message': 'SUCCEED',
                    'data': article
                })
            })
            .catch(err => {
                res.json({
                    'status': 500,
                    'message': 'ERROR'
                })
            })

    },

    delete: (req, res) => {
        const articleId = req.params.id
        Article.destroy({
            where: {
                id: articleId
            }
        })
            .then(response => {
                res.json({
                    'status': 200,
                    'message': 'SUCCEED',
                    'data': response
                })
            })
    },

    async getArticleDetailPage(req, res) {
        try {
            console.log(req.params.slug);
            const article = await Article.findOne({
                where: {
                    slug: req.params.slug,
                },
            });
            const new_articles = await Article.findAll({
                order: [["createdAt", "DESC"]],
                limit: 5,
            });
            res.render("articlesdetail", {
                article: article,
                new_articles: new_articles,
                user: req.user
            });
        } catch (err) {
            console.log(err);
        }
    },

    async getArticlesPage(req, res) {
        const q = req.query.q ? req.query.q : "";
        try {
            const articles = await Article.findAll({
                order: [["createdAt", "DESC"]],
                where: {
                    title: sequelize.where(
                        sequelize.fn("LOWER", sequelize.col("title")),
                        "LIKE",
                        "%" + q.toLowerCase() + "%"
                    ),
                },
            });
            res.render("articles", {
                articles: articles,
                q: q,
                user: req.user
            });
        } catch (err) {
            console.log(err);
        }
    }
}
