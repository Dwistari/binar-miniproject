const { Note } = require('../models')
const { Task } = require('../models')
const { Category } = require('../models');
const { Reward } = require('../models');
const { user } = require('../models')
const { family } = require('../models')
const apiRespon = require ("../handler/responHandler");

module.exports = {

    //User
    getUserFamily: (req,res) =>{
        family.findOne({
            where :{
                id : req.user.familyId
            }
        })
        .then(response => {
            if (response != null) {
                return apiRespon.success(res,response)
            } else {
                return apiRespon.notFound(res)
            }

        })
        .catch(err => {
            return apiRespon.failed(res,err)
        })
    },

    getUserData: (req,res) =>{
        user.findOne({
            where :{
                id : req.user.id
            }
        }) 
        .then(response => {
            if (response != null) {
                return apiRespon.success(res,response)
            } else {
                return apiRespon.notFound(res)
            }

        })
        .catch(err => {
            return apiRespon.failed(res,err)
        })
    },

    getUserChild: (req, res) => {
        user.findAll({
            where: {
                roleId: 2,
                familyId : req.user.familyId
            }
        })
            .then(response => {
                if (response.length !== 0) {
                    return apiRespon.success(res,response)
                } else {
                    return apiRespon.notFound(res)
                }

            })
            .catch(err => {
                return apiRespon.failed(res,err)
            })
    },

    getUserChildDetail: (req, res) => {
        let childName = req.params.name
        user.findOne({
            where: {
                name : childName
            }
        })
            .then(response => {
                if (response !== null ) {
                    return apiRespon.success(res,response)
                } else {
                    return apiRespon.notFound(res)
                }

            })
            .catch(err => {
                return apiRespon.failed(res,err)
            })
    },

    updateUser: (req, res) => {
        const query = {
            where: { id: req.params.id }
        }
        const { username, email, password, avatar, familyId,point } = req.body;
        user.update({
            username,
            email,
            password,
            avatar,
            familyId,
            point
        }, query)
            .then(user => {
                return apiRespon.success(res,user)
            })
            .catch(err => {
                return apiRespon.failed(res,err)
            })

    },

    deleteChild: (req, res) => {
        const childId = req.params.id
        user.destroy({
            where: {
                id: childId
            }
        })
            .then(response => {
                return apiRespon.success(res,response)
            })
    },
    
    // NOTES
    getNote: (req, res) => {
        Note.findAll({
            where: {
                userId: req.user.id
            },
            order: [["createdAt", "DESC"]]
        })
            .then(response => {
                if (response !== 0) {
                    return apiRespon.success(res,response)
                } else {
                    return apiRespon.notFound(res)
                }

            })
            .catch(err => {
                return apiRespon.failed(res,err)
            })
    },

    getDetailNote: (req, res) => {
        const noteId = req.params.id
        Note.findOne({
            where: {
                id: noteId
            }
        })
            .then(response => {
                return apiRespon.success(res,response)
            })
            .catch(err => {
                return apiRespon.failed(res,err)
            })
    },

    createNote: (req, res) => {
        const { title, description, label, image, pin } = req.body;
        const userId = req.user.id
        Note.create({
            userId,
            title,
            description,
            label,
            image,
            pin
        })
            .then(notes => {
                return apiRespon.success(res,notes)
            })
            .catch(err => {
                return apiRespon.failed(res,err)
            })
    },

    updateNotes: (req, res) => {
        const query = {
            where: { id: req.params.id }
        }
        const { title, description, label, image, pin } = req.body;
        const userId = req.user.id
        Note.update({
            userId,
            title,
            description,
            label,
            image,
            pin
        }, query)
            .then(notes => {
                return apiRespon.success(res,notes)
            })
            .catch(err => {
                return apiRespon.failed(res,err)
            })

    },

    deleteNotes: (req, res) => {
        const noteId = req.params.id
        Note.destroy({
            where: {
                id: noteId
            }
        })
            .then(response => {
                return apiRespon.success(res,response)
            })
            .catch(err => {
                return apiRespon.failed(res,err)
            })
    },

    // TASK

    getTask: (req, res) => {
        let condition;
        if (req.user.roleId == 2) {
            condition = {
                asignFor: req.user.username,
                familyId : req.user.familyId
            }
        }else{
            condition = {
                userId: req.user.id,
                familyId : req.user.familyId
            }
        }
        Task.findAll({
            where: condition,
            order: [["createdAt", "DESC"]]
        })
            .then(response => {
                if (response.length !== 0) {
                    return apiRespon.success(res,response)
                } else {
                    return apiRespon.notFound(res)
                }

            })
            .catch(err => {
                return apiRespon.failed(res,err)
            })
    },
    
    getTaskChild: (req, res) => {
        childId = req.params.id
        Task.findAll({
            where: {
                userId: childId,
                familyId : req.user.familyId
            }
        })
            .then(response => {
                if (response.length !== 0) {
                    return apiRespon.success(res,response)
                } else {
                    return apiRespon.notFound(res)
                }

            })
            .catch(err => {
                return apiRespon.failed(res,err)
            })
    },
    
    getDetailTask: (req, res) => {
        const taskId = req.params.id
        Task.findOne({
            where: {
                id: taskId
            }
        })
            .then(response => {
                return apiRespon.success(res,response)
            })
    },

    createTask: (req, res) => {
        const { categoryName, title, deadline, point, completed, completedDate,asignFor } = req.body;
        const userId = req.user.id
        const familyId = req.user.familyId
        Task.create({
            userId,
            familyId,
            categoryName,
            title,
            deadline,
            point,
            completed,
            completedDate,
            asignFor
        })
            .then(task => {
                return apiRespon.success(res,task)
            })
            .catch(err => {
                return apiRespon.failed(res,err)
            })
    },

    updateTask: (req, res) => {
        const query = {
            where: { id: req.params.id }
        }

        const { categoryName, title, deadline, point, completed, completedDate,asignFor } = req.body;
        Task.update({
            categoryName,
            title,
            deadline,
            point,
            completed,
            completedDate,
            asignFor
        }, query)
            .then(task => {
                return apiRespon.success(res,task)
            })
            .catch(err => {
                return apiRespon.failed(res,err)
            })

    },

    deleteTask: (req, res) => {
        const taskId = req.params.id
        Task.destroy({
            where: {
                id: taskId
            }
        })
            .then(response => {
                return apiRespon.success(res,response)
            })
            .catch(err => {
                return apiRespon.failed(res,err)
            })
    },

    searchTask: (req, res) => {
        let condition;
        if (req.user.roleId == 2) {
            condition = {
                title: req.params.title,
                asignFor: req.user.username,
                familyId : req.user.familyId
            }
        }else{
            condition = {
                title: req.params.title,
                userId: req.user.id,
                familyId : req.user.familyId
            }
        }
        Task.findAll({
            where: condition
        })
            .then(response => {
                if (response.length !== 0) {
                    return apiRespon.success(res,response)
                } else {
                    return apiRespon.notFound(res)
                }
            })
            .catch(err => {
                return apiRespon.failed(res,err)
            })
    },

    searchTaskByDate: (req, res) => {
        let condition;
        if (req.user.roleId == 2) {
            condition = {
                deadline: req.params.date,
                asignFor: req.user.username,
                familyId : req.user.familyId
            }
        }else{
            condition = {
                deadline: req.params.date,
                userId: req.user.id,
                familyId : req.user.familyId
            }
        }
        Task.findAll({
            where: condition
        })
            .then(response => {
                if (response.length !== 0) {
                    return apiRespon.success(res,response)
                } else {
                    return apiRespon.notFound(res)
                }
            })
            .catch(err => {
                return apiRespon.failed(res,err)
            })
    },

    searchTaskByCategory: (req, res) => {
        const category = req.params.category;
        const familyId = req.user.familyId
        Task.findAll({
            where: {
                categoryName: category,
                familyId: familyId,
            }
        })
            .then(response => {
                if (response.length !== 0) {
                    return apiRespon.success(res,response)
                } else {
                    return apiRespon.notFound(res)
                }
            })
            .catch(err => {
                return apiRespon.failed(res,err)
            })
    },

    // REWARD

    createReward: (req, res) => {
        const { namaReward, poin, claim } = req.body;
        const userId = req.user.id
        const familyId = req.user.familyId
        Reward.create({
            userId,
            familyId,
            namaReward,
            poin,
            claim
        })
            .then((response) => {
                return apiRespon.success(res,response)
            })
            .catch((err) => {
                return apiRespon.failed(res,err)
            });
    },

    getAllReward: (req, res) => {
        const familyId = req.user.familyId
        Reward.findAll({
            where: {
                claim: false,
                familyId : familyId
            },
            order: [["createdAt", "DESC"]]
        })
            .then((response) => {
                if (response.length != 0) {
                    return apiRespon.success(res,response)
                } else {
                    return apiRespon.notFound(res)
                }
            })
            .catch((err) => {
                return apiRespon.failed(res,err)
            });
    },

    getRewardClaimed: (req, res) => {
        const familyId = req.user.familyId
        Reward.findAll({
            where: {
                claim: true,
                familyId : familyId
            },
            order: [["createdAt", "DESC"]]
        })
            .then((response) => {
                if (response !== 0) {
                    return apiRespon.success(res,response)
                } else {
                    return apiRespon.notFound(res)
                }
            })
            .catch((err) => {
                return apiRespon.failed(res,err)
            });
    },

    getRewardById: (req, res) => {
        const rewardId = req.params.id;

        Reward.findOne({
            where: {
                id: rewardId,
            },
        })
            .then((response) => {
                return apiRespon.success(res,response)
            })
            .catch((err) => {
                return apiRespon.failed(res,err)
            });
    },

    updateReward: (req, res) => {
        const query = {
            where: {
                id: req.params.id,
            },
        };
        const { namaReward, poin, claim,claimBy,claimDate} = req.body;
        Reward.update({
                namaReward,
                poin,
                claim,
                claimBy,
                claimDate
            },
            query
        )
            .then((response) => {
                return apiRespon.success(res,response)
            })
            .catch((err) => {
                return apiRespon.failed(res,err)
            });
    },

    deleteReward: (req, res) => {
        const rewardId = req.params.id;
        Reward.destroy({
            where: {
                id: rewardId,
            },
        }).then((response) => {
            return apiRespon.success(res,response)
        })
        .catch((err) => {
            return apiRespon.failed(res,err)
        });
    },

    // Category
    getAllCategory: (req, res) => {
        Category.findAll({})
            .then(response => {
                if (response !== 0) {
                    return apiRespon.success(res,response)
                } else {
                    return apiRespon.notFound(res)
                }
            })
            .catch(err => {
                return apiRespon.failed(res,err)
            })
    },

    getCategory: (req, res) => {
        const categoryId = req.params.id
        Category.findOne({
            where: {
                id: categoryId
            }
        })
            .then(response => {
                return apiRespon.success(res,response)
            })
            .catch(err => {
                return apiRespon.failed(res,err)
            })
    },

    createCategory: (req, res) => {
        const { name, imageUrl } = req.body;
        Category.create({
            name,
            imageUrl
        })
            .then(response => {
                return apiRespon.success(res,response)
            })
            .catch(err => {
                return apiRespon.failed(res,err)
            })
    },

    updateCategory: (req, res) => {
        const query = {
            where: { id: req.params.id }
        }

        const { name, imageUrl } = req.body;
        Category.update({
            name,
            imageUrl
        }, query)
            .then(response => {
                return apiRespon.success(res,response)
            })
            .catch(err => {
                return apiRespon.failed(res,err)
            })

    },

    deleteCategory: (req, res) => {
        const categoryId = req.params.id
        Category.destroy({
            where: {
                id: categoryId
            }
        })
            .then(response => {
                return apiRespon.success(res,response)
            })
            .catch(err => {
                return apiRespon.failed(res,err)
            })
    }
}
