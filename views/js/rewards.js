const url = `http://localhost:3000`

$(document).ready(async function(){
    console.log('ready');
    tampilkanListReward();
    tampilkanListRewardClaimed();
    // tab
    handlingTab();

    // -----------------------------
    // ngambil data user yg login
    // -----------------------------
    userData = await getUser();
    console.log(userData);
   

    // tambah reward
    $("#tombol_tambah").on('click', function(){
        console.log($("#nama_reward").val());
        console.log($("#poin").val());
        tambahReward();
    })

    // hapus reward
    $("#iniModal").on('show.bs.modal', function(e){
        const button = $(e.relatedTarget);
        const id = button.attr("data-id");
        console.log (id);
        $("#button_hapus").on('click', function(){
            hapusReward(id);
        })
    })

    //update reward
    $("#itiModal").on('show.bs.modal', function(e){
        const button = $(e.relatedTarget);
        const id = button.attr("data-id");
        const currTitle = $(`#detail-${id}`).find('.reward-title').html();
        const currPoint = $(`#detail-${id}`).find('.reward-point').html();
        $('#edit_title_form').val(currTitle);
        $('#edit_point_form').val(currPoint);
        $("#button_edit").on('click', function(){
            updateReward(id);
        })
    })

    //claim reward
    // $('#reward_list').on('click', '.button_check', function(){
    //     const id = $(this).attr('data-id');
    //     let poinUser = userData.point;
    //     let poinReward = $(`#detail-${id}`).find('.reward-point').html();
    //     let poinTotal = poinUser - poinReward;
    //     poinReward = parseInt(poinReward);
    //     console.log(poinUser);
    //     console.log(poinReward);
    //     if(poinUser < poinReward){
    //         $('#modal-kurang').modal('show');
    //     }
    //     else{
    //         claimReward(id, userData.id, userData.name, poinTotal );
    //         $('#modal_sukses_claim').modal('show');
    //     }
      
    // })
    $("#modal_sukses_claim").on('hide.bs.modal', function(){
      window.location.reload();  
    })


}) 

function handlingTab() {
    $('#tab-rewards').css("background-color", "#EFBD4A");
    $('#tab-claim').css("background-color", "#F9E4B5");

    $('#tab-rewards').on('click', function () {
        $('#tab-rewards').css("background-color", "#EFBD4A");
        $('#tab-claim').css("background-color", "#F9E4B5");
        $('#reward_list').removeClass("d-none");
        $('#reward_list_claimed').addClass("d-none");
    }),

    $('#tab-claim').on('click', function () {
        $('#tab-rewards').css("background-color", "#F9E4B5");
        $('#tab-claim').css("background-color", "#EFBD4A");
        $('#reward_list_claimed').removeClass("d-none");
        $('#reward_list').addClass("d-none");
    })
}


async function getUser(){
    const response = await fetch(
        `${url}/api/user`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      const data = await response.json();
      return data.data;
}

function tambahReward(){
    $.ajax({
        url: `${url}/api/rewards/create`,
        type: "post",
        data: {
            namaReward: $("#nama_reward").val(),
            poin: $("#poin").val(),
            claim: false,
        },
        success: function(result){
            $('#exampleModal').modal('hide');
            window.location.reload();
        },
        error: function (err) {
            console.log(err);
        }
    })
    
}

function updateReward(id){
    $.ajax({
        url: `${url}/api/rewards/update/${id}`,
        type: "put",
        data: {
            namaReward: $("#edit_title_form").val(),
            poin: $("#edit_point_form").val(),
        },
        success: function(result){
            $('#itiModal').modal('hide');
            window.location.reload();
        },
        error: function (err) {
            console.log(err);
        }
    })
    
}

function hapusReward(id){
    $.ajax({
        url: `${url}/api/rewards/delete/${id}`,
        type: "delete",
        success: function (result) {
            $('#iniModal').modal('hide')
            window.location.reload()
        },
        error: function (err) {
            console.log(err);
        }

    })
}

async function claimReward(id, userid, claimedBy, point) {
    var currentDate = new Date();
    var dd = String(currentDate.getDate()).padStart(2, '0');
    var mm = String(currentDate.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = currentDate.getFullYear();

    currentDate = yyyy + '-' + mm + '-' + dd;

    console.log(currentDate)
    const response = await fetch(
        `${url}/api/rewards/update/${id}`,
        {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                claim: true,
                claimBy: claimedBy,
                claimDate: currentDate,
            })
        }
    ).then(response=>{console.log("sukses update rewards")})
    
    const resUser = await fetch(
        `${url}/api/user/update/${userid}`,
        {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                point: point,
            })
        },
        

    ).then(resUser => {
        console.log("sukses udpdate user");

        // window.location.reload()
    });

    
}


function tampilkanListReward() {
    $("#reward_list").html('');
    $.ajax({
      url: `${url}/api/rewards/list`,
      type: "get",
      datatype: "json",

      success: function (result) {
          if (result.status != 200) {
              $('#reward_list').append(`<div class="position-relative" style="min-height: calc(100vh - 350px)">
              <div class="position-absolute top-50 start-50 translate-middle text-center"><img
                      src="/static/images/users/cuate.png" alt="">
                  <p class="card-subtitle fc-text-arrow mt-2 fs-4 fw-bold">Wah belum ada reward saat ini</p>
              </div>
          </div>`)
          }
          else{
            $.each(result.data, function(i, data){
                $("#reward_list").append(
                  `
                <div class="row mb-3 bg-white" class="reward-item">
                              <div class="col-md-2 p-4 text-center d-flex align-items-center justify-content-center" style="background-color:#FFF">
                                  <div class="user text-center text-dark" style="font-family: 'DM Sans', sans-serif;">
                                      <h2 class="bi bi-pin-fill mt-4"> </h2>
                                  </div>
                              </div>
                              <div class="col-md-7 p-3 mt-3" id="detail-${data.id}" style="height: 112px; width: 1109pxpx">
                                  <p class="m-0 p-0 fs-4 fw-bold reward-title">${data.namaReward}</p> <!---nama reward-->
                                  <p class="mt-2 reward-point">${data.poin}</p><!---poin-->
  
                              </div>
                              <div class="col-md-3 p-5">
                                  <div class="row">
                                      <div class="col-md-4 ml-2">
                                          <!-- Button trigger modal Edit -->
                                          <button type="button" class="btn bi bi-pencil-fill button_edit" data-bs-toggle="modal"
                                              data-bs-target="#itiModal" data-id=${data.id}>
                                          </button>
                                      </div>
                                      <div class="col-md-4 ml-2">
                                          <!-- Button trigger modal Trash -->
                                          <button type="button" class="btn bi bi-trash button_hapus" data-bs-toggle="modal"
                                              data-bs-target="#iniModal" data-id=${data.id}>
                                          </button>
                                      </div>
  
  
                                     
                                  </div>
                              </div>
                          </div>
  
                `
                );
            })
          }
          
      },
    });
}

function tampilkanListRewardClaimed() {
    $("#reward_list_claimed").html('');
    $.ajax({
      url: `${url}/api/rewards/list/claimed`,
      type: "get",
      datatype: "json",

      success: function (result) {
          console.log(result.data);
          if (result.data.length==0){
            $('#reward_list_claimed').append(`<div class="position-relative" style="min-height: calc(100vh - 350px)">
            <div class="position-absolute top-50 start-50 translate-middle text-center"><img
                    src="/static/images/users/cuate.png" alt="">
                <p class="card-subtitle fc-text-arrow mt-2 fs-4 fw-bold">Wah belum ada reward saat ini</p>
            </div>
        </div>`)
        }
        else{
            $.each(result.data, function(i, data){
                let date = convertDate(data.claimDate);
                $("#reward_list_claimed").append(
                  `
                <div class="row mb-3 bg-white" class="reward-item w-100">
                              <div class="col-md-1 p-4 text-center d-flex align-items-center">
                                  <div class="user text-center text-dark" style="font-family: 'DM Sans', sans-serif;">
                                      <h2 class="bi bi-pin-fill" width="50"></h2>
                                  </div>
                              </div>
                              <div class="col-md-7 p-3" id="detail-claimed-${data.id}" style="height: 112px; width: 1109pxpx">
                                  <p class="m-0 p-0 fs-4 fw-bold reward-title-claimed">${data.namaReward}</p> <!---nama reward-->
                                  <p class="mt-2 reward-point">${data.poin}</p><!---poin-->
  
                              </div>
                              <div class="col-md-3 p-5">
                                  <div class="row">
                                      <div class="col-md-2">
                                          <h2 class="btn bi bi-check-circle">
                                          </h2>
                                      </div>
                                      <div class="col-md-10">
                                          <b class="mt-2">Diklaim oleh ${data.claimBy}</b>
                                          <p>${date}</p>
                                      </div>                                   
                                  </div>
                              </div>
                          </div>
  
                `
                );
            })
        }
          
      },
    });
}

function convertDate(inputDate) {
    const monthNames = ["-","Januari", "Februari", "Maret", "April", "Mei", "Juni",
        "Juli", "Agustus", "September", "Oktober", "November", "Desember"
    ];
    let date = new Date(inputDate);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let dt = date.getDate();

    if (dt < 10) {
        dt = '0' + dt;
    }

    return dt + '-' + monthNames[month] + '-' + year
}
