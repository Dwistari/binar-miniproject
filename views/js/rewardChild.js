var userData;
const LINK = `http://localhost:3000`


$(async function () {
    
    await getDataUser()
    $('#poin_user').append(`${userData.point} Pt`);
    getDataReward()
    handlingTab()
})

async function getDataUser() {
    let url = `${LINK}/api/user`;
    try {
        const response = await fetch(url);
        var res = await response.json();
        userData = res.data
        console.log("userData",userData)
    } catch (error) {
        console.log(error);
    }
}


async function getDataReward() {
    let url = `${LINK}/api/rewards/list`;
    try {
        const response = await fetch(url);
        var res = await response.json();
        showGetRewards(res,"all");

    } catch (error) {
        console.log(error);
    }
}

async function getDataRewardClaimed() {
    let url = `${LINK}/api/rewards/list/claimed`
    try {
        const response = await fetch(url);
        var res = await response.json();
        showGetRewards(res,"claim");

    } catch (error) {
        console.log(error);
    }
}

function showGetRewards(res,type) {
    let html = '';
    let htmlSegment = ""
    let body = ''
    let claim = ''
    let claimed = ''
    if (res.status == 200) {
        if (res.data.length == 0) {
            html = `<div class="position-relative" style="min-height: calc(100vh - 350px)">
            <div class="position-absolute top-50 start-50 translate-middle text-center"><img
                    src="/static/images/users/cuate.png" alt="">
                <p class="card-subtitle fc-text-arrow mt-2 fs-4 fw-bold">Wah belum ada reward saat ini</p>
            </div>
        </div>`
        }else{
            res.data.forEach((it, index) => {
                console.log("data_rewards", it)
                body = `<div class="row mb-3 bg-white">
                <div class="col-2" style="background-color:#FFFFFF; border-radius: 30px;">
                    <div class="user text-center text-dark p-0 pt-4 mt-2"
                        style="font-family: 'DM Sans', sans-serif;">
                        <h1><i class="bi bi-pin-angle-fill"></i></h1>
                    </div>
                </div>
                <div class="col-md-7 p-3">
                    <p class="m-0 p-0 fs-4 fw-bold">${it.namaReward}</p>
                    <p class="mt-2">${it.poin}pt</p>
                </div>`
    
                claim =
                    `<div class="col-md-3 p-4">
                    <div class="row g-0">
                        <div class="btn col-2 p-3 text-center text-white position-absolute"
                            style="background-color:#5B8F9A" onClick="claimReward(${it.id},${it.poin})">
                            Claim Reward
                        </div>
                    </div>
                </div>
                 </div>`
    
                claimed = ` <div class="col-md-3 p-4">
            <div class="row g-0">
                <div class="btn col-2 p-3 text-center text-dark position-absolute"
                    style="background-color:#E9E9E9">
                    Claim Reward
                </div>
            </div>
            </div>
        </div>`
                console.log("userData.point",userData.point)
                console.log("data.point",it.poin)

                if (type == "claim") {
                    var date = convertDate(it.claimDate)
                    claimed = `<div class="col-md-3 p-4">
                    <div class="row">
                    <button type="button" class="btn bi bi-check-circle">
                        <b class="mt-2">Diklaim oleh ${it.claimBy}</b>
                        <p>${date}</p>
                    </div>
                    </div>
                </div>`

                htmlSegment = body + claimed
                }else{
                    if (userData.point >= it.poin) {
                        htmlSegment = body + claim
                    }else{
                        htmlSegment = body + claimed
                    }
                }
                
                html += htmlSegment;
            })
        }
    }else{
        html = `<div class="position-relative" style="min-height: calc(100vh - 350px)">
        <div class="position-absolute top-50 start-50 translate-middle text-center"><img
                src="/static/images/users/cuate.png" alt="">
            <p class="card-subtitle fc-text-arrow mt-2 fs-4 fw-bold">Wah belum ada reward saat ini</p>
        </div>
    </div>`
    }

    let rewards = document.querySelector('#rewards-list');
    rewards.innerHTML = html;
}

async function claimReward(id,poin) {
    var userid = userData.id;
    var currentDate = new Date();
    var dd = String(currentDate.getDate()).padStart(2, '0');
    var mm = String(currentDate.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = currentDate.getFullYear();

    currentDate = yyyy + '-' + mm + '-' + dd;

    let poinUser = userData.point
    let totalPoin = poinUser - poin

    const response = await fetch(
        `${LINK}/api/rewards/update/` + id,
        {
            method: "PUT",
            // baseURL: "",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                claim: true,
                claimBy: userData.username,
                claimDate : currentDate
            })
        }
    ).then(response => {
        console.log("sukses update rewards");
    });
    const resUser = await fetch (
        `${LINK}/api/user/update/` + userid,
        {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                point:totalPoin,
            })
        }
        ).then(resUser=>{
            console.log('sukses update user');
            window.location.reload();
        })
}

async function updatePoinUser(id,poinReward) {
    let poinUser = userData.point
    let totalPoin = poinUser - poinReward

    const response = await fetch(
        `${LINK}/api/user/update/` + id,
        {
            method: "PUT",
              // baseURL: "",
              headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                point : totalPoin
            })
          }
        ).then(response => {
             window.location.reload()
      });
  
}

function handlingTab() {
    $('#tab-rewards').css("background-color", "#EFBD4A");
    $('#tab-claim').css("background-color", "#F9E4B5");

    $('#tab-rewards').on('click', function () {
        $('#tab-rewards').css("background-color", "#EFBD4A");
        $('#tab-claim').css("background-color", "#F9E4B5");
        getDataReward()
    }),

    $('#tab-claim').on('click', function () {
        $('#tab-rewards').css("background-color", "#F9E4B5");
        $('#tab-claim').css("background-color", "#EFBD4A");
        getDataRewardClaimed()

    })
}

function convertDate(inputDate) {
    const monthNames = ["-", "Januari", "Februari", "Maret", "April", "Mei", "Juni",
        "Juli", "Agustus", "September", "Oktober", "November", "Desember"
    ];
    let date = new Date(inputDate);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let dt = date.getDate();

    if (dt < 10) {
        dt = '0' + dt;
    }

    return dt + '-' + monthNames[month] + '-' + year
}
