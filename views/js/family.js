const BASE_URL = `http://localhost:3000`

$(function () {
    getDataUser()
    getDataTask()
    getDataChilds()
    deletedTask()
    getDataCategory()
    generateTaskForUpdate()
})


async function getDataUser() {
    let url =`${BASE_URL}/api/user`;
    try {
        const response = await fetch(url);
        let res = await response.json();
        let userData = res.data

        if (userData.roleId == 2) {
            document.querySelector('.addmember').style.display = "none";
        }else{
            document.querySelector('.addmember').style.display = "block";
        }

    } catch (error) {
        console.log(error);
    }
}

async function getDataChilds() {
  let url = `${BASE_URL}/api/user/childs`;
  try {
    const response = await fetch(url);
    var res = await response.json();
    showChilds(res);
    showListChild(res);
    getFamily(res.data.id);
  } catch (error) {
    console.log(error);
  }
}

async function getFamily() {
  let url = `${BASE_URL}/api/family`;
  try {
    const response = await fetch(url);
    var res = await response.json();
    document.getElementById('family-name').innerHTML = 'Keluarga ' + res.data.familyname;
  } catch (error) {
    console.log(error);
  }
}

async function getDataCategory() {
  let url = `${BASE_URL}/api/category/list`;
  try {
    const response = await fetch(url);
    var res = await response.json();
    showCategorySelected(res.data);
  } catch (error) {
    console.log(error);
  }
}

function showCategorySelected(categoryName) {
  let dropdown = $('.category-name');
  dropdown.empty();

  dropdown.append('<option selected="true" disabled>Pilih kategori</option>');
  dropdown.prop('selectedIndex', 0);
  $.each(categoryName, function (key, category) {
    dropdown.append($('<option></option>').attr('value', category.name).text(category.name));
  });

  let categoryUpdateList = $('.update-category');
  categoryUpdateList.empty();

  categoryUpdateList.append('<option selected="true" disabled>Pilih kategori</option>');
  categoryUpdateList.prop('selectedIndex', 0);
  $.each(categoryName, function (key, category) {
    categoryUpdateList.append($('<option></option>').attr('value', category.name).text(category.name));
  });
}

function showChilds(res) {
  let html = '';
  let htmlSegment = '';

  res.data.forEach((it, index) => {
    htmlSegment = `<div class="text-center pt-3 pb-1 mx-4">
        <img src="/static/images/users/Anak.png" class="rounded-circle" width="100">
        <p class="nameMember pt-1" style="font-weight: bold;">${it.name} </p>
        <p class="poinMember pb-1">${it.point} Pt</p>
    </div>`;

    html += htmlSegment;
  });

  let task = document.querySelector('#child-list');
  task.innerHTML = html;
}

function showListChild(res) {
  let asign = $('#update-asign');
  asign.empty();

  asign.append('<option selected="true" disabled>Pilih anak</option>');
  asign.prop('selectedIndex', 0);
  $.each(res.data, function (key, childs) {
    console.log(childs.username);
    asign.append($('<option></option>').attr('value', childs.username).text(childs.username));
  });
}

async function getDataTask() {
  let url = `${BASE_URL}/api/task/list`;
  try {
    const response = await fetch(url);
    var res = await response.json();
    console.log('getDataTask' + res + '->' + response);
    showTaskData(res);
  } catch (error) {
    console.log(error);
  }
}

function showTaskData(res) {
  let html = '';
  let htmlSegment = '';
  let viewTask = '';
  let onprogress = '';
  let doneTask = '';
  let deadline;
  let completedDate;

  if (res.status != 200) {
    html = `<div class="position-relative" style="min-height: calc(80vh - 350px)">
        <div class="position-absolute top-50 start-50 translate-middle text-center"><img
                src="/static/images/users/cuate.png" alt="">
            <p class="card-subtitle fc-text-arrow mt-2 fs-4 fw-bold">Wah belum ada task saat ini,<br />
                mulai
                tambahin yuk!</p>
        </div>
    </div>`;
  } else {
    if (res.data == 'undefined') {
      html = `<div class="position-relative" style="min-height: calc(100vh - 350px)">
            <div class="position-absolute top-50 start-50 translate-middle text-center"><img
                    src="/static/images/users/cuate.png" alt="">
                <p class="card-subtitle fc-text-arrow mt-2 fs-4 fw-bold">Wah belum ada task saat ini,<br />
                    mulai
                    tambahin yuk!</p>
            </div>
        </div>`;
    } else {
      res.data.forEach((it, index) => {
        deadline = convertDate(it.deadline);
        completedDate = convertDate(it.completedDate);
        viewTask = `<div class="row mb-3 bg-white task-item">
                                <div class="col-md-2 p-4 text-center" style="background-color:#5B8F9A">
                                    <div class="user text-center text-dark" style="font-family: 'DM Sans', sans-serif;">
                                        <img src="/static/images/users/Anak.png" class="rounded-circle" width="50">
                                        <p class="card-title mt-2 fw-normal fs-5 text-white" id="asign-name">${it.asignFor}</p>
                                    </div>
                                </div>
                                <div class="col-md-7 p-3" style="height: 112px; width: 1109pxpx">
                                    <p class="m-0 p-0 fs-4 fw-bold">${it.title}</p>
                                    <p class="mt-2">${it.categoryName} &#x2022; ${it.point}pt,</p>
                                    <p class="mt-2"><i class="bi-alarm"></i> ${deadline}</p>
                                </div>
                              `;
        onprogress = `<div class="col-md-3 p-5">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <button type="button" class="btn bi bi-check-lg fs-5"  data-id=${it.asignFor} onClick="completedTask(${it.id},${it.point},'${it.asignFor}')">
                                            </button>
                                        </div>
                                        <div class="col-md-4 ml-2">
                                            <!-- Button trigger modal Edit -->
                                            <button type="button" class="btn bi bi-pencil-fill fs-5" data-bs-toggle="modal"
                                            data-id=${it.id} data-bs-target="#itiModal">
                                            </button>
        
                                         
                                        </div>
                                        <div class="col-md-4 ml-2">
                                            <!-- Button trigger modal Trash -->
                                            <button type="button" class="btn bi bi-trash fs-5 delete-task" data-id="${it.id}" data-bs-toggle="modal"
                                                data-bs-target="#deletedModal" >
                                            </button>
                                        </div>
                                    </div>
                                    </div>
                                </div> `;

        doneTask = `<div class="col-md-3 p-4">
                <div class="row">
                    <b class="mt-2">Selesai</b>
                    <p class="mt-2"><i class="bi-alarm"></i> ${completedDate}</p>
                </div>
                </div>
            </div>`;
        if (it.completed == true) {
          htmlSegment = viewTask + doneTask;
        } else {
          htmlSegment = viewTask + onprogress;
        }

        html += htmlSegment;
      });
    }
  }

  let task = document.querySelector('.taskContainer');
  task.innerHTML = html;
}

function deletedTask() {
  $('#deletedModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var id = button.attr('data-id');
    var url = 'api/task/delete/' + id;

    $(document).on('click', '#hapus-task', function () {
      $.ajax({
        url: url,
        type: 'DELETE',
        success: function (result) {
          $('#iniModal').modal('hide');
          window.location.reload();
        },
        error: function (err) {
          console.log(err);
        },
      });
    });
  });
}

function generateTaskForUpdate() {
  $('#itiModal ').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var id = button.attr('data-id');
    getTaskById(id);
    $(document).on('click', '#update-task', function () {
      validatForm(id);
    });
  });
}

async function getTaskById(id) {
  let url = `${BASE_URL}/api/task/` + id;
  try {
    const response = await fetch(url);
    var res = await response.json();
    bindDataTask(res);
  } catch (error) {
    console.log(error);
  }
}

function bindDataTask(res) {
  $('.update-category').append(`<option selected="true">${res.data.categoryName}</option>`);
  $('.update-title').val(res.data.title);
  $('.deadline').val(res.data.deadline);
  $('.input-point').val(res.data.point);
  $('#update-asign').val(res.data.asignFor);
}

async function updateTask(id) {
  const response = await fetch(`${BASE_URL}/api/task/update/` + id, {
    method: 'PUT',
    // baseURL: "",
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      categoryName: $('.update-category').val(),
      title: inputTitle,
      deadline: $('.deadline').val(),
      point: $('.input-point').val(),
      asignFor: $('#update-asign').val(),
    }),
  }).then((response) => {
    window.location.reload();
  });
}

async function completedTask(id, point, name) {
  var currentDate = new Date();
  var dd = String(currentDate.getDate()).padStart(2, '0');
  var mm = String(currentDate.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = currentDate.getFullYear();

  currentDate = yyyy + '-' + mm + '-' + dd;

  const response = await fetch(`${BASE_URL}/api/task/update/` + id, {
    method: 'PUT',
    // baseURL: "",
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      completed: true,
      completedDate: currentDate,
    }),
  }).then((response) => {
    getUserChildId(name, point);
  });
}

async function getUserChildId(name, point) {
  let url = `${BASE_URL}/api/user/detail/` + name;
  try {
    const response = await fetch(url);
    var res = await response.json();
    let pointChild = res.data.point;
    console.log('getUserChildId', res);

    updatePoinUser(point, pointChild, res.data.id);
  } catch (error) {
    console.log(error);
  }
}

async function updatePoinUser(poinReward, pointChild, id) {
  let totalPoin = pointChild + poinReward;

  const response = await fetch(`${BASE_URL}/api/user/update/` + id, {
    method: 'PUT',
    // baseURL: "",
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      point: totalPoin,
    }),
  }).then((response) => {
    window.location.reload();
  });
}

function validatForm(id) {
  var a = $('.update-category').val();
  var b = $('.update-title').val();
  var c = $('.deadline').val();
  var d = $('.input-point').val();
  var e = $('#update-asign').val();
  let alert = document.getElementById('alert-update');

  titleCase(b);

  if (a == null || (b.length == 0 && c.length == 0) || c.length == 0 || d.length == 0 || e == null) {
    console.log('warning');
    alert.style.display = 'block';
  } else {
    alert.style.display = 'none';
    updateTask(id);
  }
}

function titleCase(str) {
  var splitStr = str.toLowerCase().split(' ');
  for (var i = 0; i < splitStr.length; i++) {
    splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  inputTitle = splitStr.join(' ');
  console.log(inputTitle);
  return splitStr.join(' ');
}

function convertDate(inputDate) {
  const monthNames = ['-','Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
  let date = new Date(inputDate);
  let year = date.getFullYear();
  let month = date.getMonth() + 1;
  let dt = date.getDate();

  if (dt < 10) {
    dt = '0' + dt;
  }

  return dt + ' ' + monthNames[month] + ' ' + year;
}
