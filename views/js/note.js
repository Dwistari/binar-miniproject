const LINK = `http://localhost:3000`

async function getNotes(pinned, unpinned){
  const response = await fetch(
    `${LINK}/api/notes/list`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  const data = await response.json();
  console.log(data);
  if(!data.data[0]){
    $('#empty_note').removeClass('d-none');
  }
  else{
    $('#empty_note').addClass('d-none');
    data.data.forEach(note => {
      console.log(note);
      if(note.pin == true) {pinned.push(note);
        console.log(pinned);}
      
      else {unpinned.push(note);
      console.log(unpinned);}
  });
  }
  
}


async function addNote(){
  const response = await fetch(
    `${LINK}/api/notes/create`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        title: $('#add_note_title').val(),
        description: $('#add_note_desc').val(),
        pin: false,
      })
    }
  );
  const data = await response.json();
  if (data.status == 200){
      window.location.reload();
  }
}

async function deleteNote(id){
  const response = await fetch(
    `${LINK}/api/notes/deleted/${id}`,
    {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    }
  )
  const data = await response.json();
  if (data.status == 200){
    window.location.reload();
  }
}

async function updateNote(id){
  const response = await fetch(
    `${LINK}/api/notes/update/${id}`,
    {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        title: $('#update_note_title').val(),
        description: $('#update_note_desc').val(),
      })
    }
  )
  const data = await response.json();
  if (data.status == 200){
    window.location.reload();
  }
}

async function updatePin(id,pinValue){
  const response = await fetch(
    `${LINK}/api/notes/update/${id}`,
    {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        pin: pinValue ,
      })
    }
  );
  const data = await response.json();
  if (data.status == 200){
      window.location.reload();
  }
  
}

$(document).ready(async function(){
    let pinnedNotes = [];
    let unpinnedNotes = [];

    console.log($(document).user);

    const note = await getNotes(pinnedNotes,unpinnedNotes);
    console.log(note);
    // console.log(unpinnedNotes);
    if(pinnedNotes || unpinnedNotes){
      pinnedNotes.forEach(note => {
        $('#note_list').append(`
        <article class="notes-item my-3">
        <div class = "d-flex justify-content-between">
          <h3>${note.title}</h3>
          <h3 class="bi bi-pin-fill"></h3>
        </div>
        <h6 class="mt-2">${note.description}</h6>
        <br>
        <br>
        <br>
        <br>
        <br>
        <p>${note.updatedAt}</p>
        <p class="note_id">${note.id}</p>
        <p class="note_pin">${note.pin}</p>
        </article>`);
      });
      unpinnedNotes.forEach(note => {
        $('#note_list').append(`
        <article class="notes-item my-3">
        <h3>${note.title}</h3>
        <h6 class="mt-2">${note.description}</h6>
        <br>
        <br>
        <br>
        <br>
        <br>
        <p>${note.updatedAt}</p>
        <p class="note_id">${note.id}</p>
        <p class="note_pin">${note.pin}</p>
        </article>`);
      });
    }
    console.log("berhasil")
   
    
    // Efek note item aktif
    $('article').on('click', function(){
        $(this).addClass('notes-item-active').siblings().removeClass('notes-item-active');
        });

    // ambil note details
    let noteID;
    let pinValue;
    let noteTitle;
    let noteDesc;
    $(".notes-item").click(function(){
        noteTitle = $(this).find('h3').html();
        noteDesc = $(this).find('h6').html();
        const lastUpdate = $(this).find('p').html();
        noteID = $(this).find('.note_id').html();
        if ($(this).find('.note_pin').html() == "true"){
          pinValue = true;
          $("#pin_button").removeClass('bi-pin');
          $("#pin_button").addClass('bi-pin-fill');
        }
        else{
          pinValue = false;
          $("#pin_button").removeClass('bi-pin-fill');
          $("#pin_button").addClass('bi-pin');
        }
        $("#notes_details").find('h2').html(noteTitle);
        $("#notes_details").find('h5').html(noteDesc);
        $("#last_updated").find('p').html(`Terakhir diubah ${lastUpdate}`);
        $("#notes_details").removeClass('d-none');
    });
    
    $("#delete_note_button").click(function(){
      deleteNote(noteID);
    });

    $("#add_note_button").click(function(){
      addNote();      
    });

    $("#pin_button").click(function(){
      if(pinValue==false){
        pinValue = true;
      }
      else {
        pinValue = false;
      }
      updatePin(noteID, pinValue);
    });

    $("#update_button").click(function(){
      console.log(noteTitle, noteDesc);
      $('#update_note_title').val(noteTitle);
      $('#update_note_desc').val(noteDesc);
      $('#update_note_button').click(function(){
        updateNote(noteID);
      })
    })
})
