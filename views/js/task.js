const LINK = `http://localhost:3000`

$(function () {
  getDataUser();
  getDataTask();
  getDataCategory();
  searchTaskByCategory();
  createTaskSubmit();
  deletedTask();
  generateTaskForUpdate();
  getDataChilds();
  searchTask();
});

var userData;
var inputTitle;

async function getDataUser() {
  let url = `${LINK}/api/user`;
  try {
    const response = await fetch(url);
    var res = await response.json();
    userData = res.data;

    if (userData.roleId == 2) {
      document.querySelector('#add-task').style.display = 'none';
    } else {
      document.querySelector('#add-task').style.display = 'block';
    }
  } catch (error) {
    console.log(error);
  }
}

async function getDataTask() {
  let url = `${LINK}/api/task/list`;
  try {
    const response = await fetch(url);
    var res = await response.json();
    console.log('getDataTask' + res + '->' + response);
    showTaskData(res);
  } catch (error) {
    console.log(error);
  }
}

async function getDataCategory() {
  let url = `${LINK}/api/category/list`;
  try {
    const response = await fetch(url);
    var res = await response.json();
    showCategory(res);
  } catch (error) {
    console.log(error);
  }
}

async function getDataChilds() {
  console.log('getDataChilds');
  let url = `${LINK}/api/user/childs`;
  try {
    const response = await fetch(url);
    var res = await response.json();
    showChilds(res);
  } catch (error) {
    console.log(error);
  }
}

async function getTaskById(id) {
  let url = `${LINK}/api/task/` + id;
  try {
    const response = await fetch(url);
    var res = await response.json();
    bindDataTask(res);
  } catch (error) {
    console.log(error);
  }
}

function bindDataTask(res) {
  $('.update-category').append(`<option selected="true">${res.data.categoryName}</option>`);
  $('.update-title').val(res.data.title);
  $('.deadline').val(res.data.deadline);
  $('.input-point').val(res.data.point);
  $('#update-asign').val(res.data.asignFor);
}

function searchTaskByCategory() {
  $(document).on('click', '#category', function () {
    let category = $(this).data('name');
    let url = 'api/task/search/category/' + category;
    $.ajax({
      url: url,
      type: 'GET',
      success: function (result) {
        showTaskData(result);
      },
      error: function (err) {
        console.log(err);
      },
    });
  });
}

function showTaskData(res) {
  let html = '';
  let htmlSegment = '';
  let viewTask = '';
  let onprogress = '';
  let onprogressChild = '';
  let doneTask = '';
  let deadline = '';
  let completedDate = '';

  if (res.status != 200) {
    html = `<div class="position-relative" style="min-height: calc(100vh - 350px)">
              <div class="position-absolute top-50 start-50 translate-middle text-center">
                <img src="/static/images/users/cuate.png" alt="">
                <p class="card-subtitle fc-text-arrow mt-2 fs-4 fw-bold">Wah belum ada task saat ini,<br />
                mulai tambahin yuk!</p>
              </div>
            </div>`;
  } else {
    if (res.data == 'undefined') {
      html = `<div class="position-relative" style="min-height: calc(100vh - 350px)">
            <div class="position-absolute top-50 start-50 translate-middle text-center"><img
                    src="/static/images/users/cuate.png" alt="">
                <p class="card-subtitle fc-text-arrow mt-2 fs-4 fw-bold">Wah belum ada task saat ini,<br />
                    mulai
                    tambahin yuk!</p>
            </div>
        </div>`;
    } else {
      res.data.forEach((it, index) => {
        deadline = convertDate(it.deadline);
        completedDate = convertDate(it.completedDate);
        viewTask = `<div class="row mb-3 bg-white task-item">
                                <div class="col-md-2 p-4 text-center" style="background-color:#5B8F9A">
                                    <div class="user text-center text-dark" style="font-family: 'DM Sans', sans-serif;">
                                        <img src="/static/images/users/Anak.png" class="rounded-circle" width="50">
                                        <p class="card-title mt-2 fw-normal fs-5 text-white">${it.asignFor}</p>
                                    </div>
                                </div>
                                <div class="col-md-7 p-3" style="height: 112px; width: 1109pxpx">
                                    <p class="m-0 p-0 fs-4 fw-bold">${it.title}</p>
                                    <p class="mt-2">${it.categoryName} &#x2022; ${it.point}pt,</p>
                                    <p class="mt-2"><i class="bi-alarm"></i> ${deadline}</p>
                                </div>
                              `;
        onprogress = `<div class="col-md-3 p-5">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <button type="button" class="btn bi bi-check-lg fs-5" onClick="completedTask(${it.id},${it.point})" >
                                            </button>
                                        </div>
                                        <div class="col-md-4 ml-2">
                                            <!-- Button trigger modal Edit -->
                                            <button type="button" class="btn bi bi-pencil-fill fs-5" data-bs-toggle="modal"
                                            data-id=${it.id}
                                                data-bs-target="#itiModal">
                                            </button>
        
                                         
                                        </div>
                                        <div class="col-md-4 ml-2">
                                            <!-- Button trigger modal Trash -->
                                            <button type="button" class="btn bi bi-trash fs-5 delete-task" data-id="${it.id}" data-bs-toggle="modal"
                                                data-bs-target="#deletedModal" >
                                            </button>
                                        </div>
                                    </div>
                                    </div>
                                </div> `;

        onprogressChild = `<div class="col-md-3 p-4">
                <div class="row g-0">
                    <div class="btn col-2 p-3 text-center text-white position-absolute"
                        style="background-color:#FFFF ; border-color:#5B8F9A; color: #5B8F9A !important" onClick="completedTask(${it.id},${it.point})" >
                       Selesai
                    </div>
                </div>
            </div>
             </div>`;

        doneTask = `<div class="col-md-3 p-4">
                <div class="row">
                    <b class="mt-2">Selesai</b>
                    <p class="mt-2"><i class="bi-alarm"></i> ${completedDate}</p>
                </div>
                </div>
            </div>`;

        if (it.completed == true) {
          htmlSegment = viewTask + doneTask;
        } else {
          if (userData.roleId == 2) {
            htmlSegment = viewTask + onprogressChild;
          } else {
            htmlSegment = viewTask + onprogress;
          }
        }

        html += htmlSegment;
      });
    }
  }

  let task = document.querySelector('#task-list');
  task.innerHTML = html;
}
  function showCategory(res) {
    let html = '';
    let htmlSegment = '';
    showCategorySelected(res.data);
    res.data.forEach((it, index) => {
      let categoryName = it.name;
      let image = '';

      if (categoryName == 'Semua') {
        image = '/static/images/users/category semua.png';
      } else if (categoryName == 'Pendidikan') {
        image = '/static/images/users/category pendidikan.png';
      } else if (categoryName == 'Belanja') {
        image = '/static/images/users/category belanja.png';
      } else if (categoryName == 'Ibadah') {
        image = '/static/images/users/category ibadah.png';
      } else if (categoryName == 'Kantor') {
        image = '/static/images/users/category kantor.png';
      } else if (categoryName == 'Kesehatan') {
        image = '/static/images/users/category kesehatan.png';
      } else if (categoryName == 'Kewajiban') {
        image = '/static/images/users/category kewajiban.png';
      } else if (categoryName == 'Kreatifitas') {
        image = '/static/images/users/category kreatifitas.png';
      } else if (categoryName == 'Olahraga') {
        image = '/static/images/users/category olahraga.png';
      } else if (categoryName == 'Perbaikan') {
        image = '/static/images/users/category perbaikan.png';
      } else if (categoryName == 'Rumah') {
        image = '/static/images/users/category rumah.png';
      }

      htmlSegment = `<div class="col-md-1" id="category" data-name=${categoryName}>
        <button type="button" class="btn position-relative rounded-circle" width="40">
            <img src="${image}" class="rounded-circle" width="40">
            <p class="card-subtitle fc-text-arrow mt-2 fs-6">${categoryName}</p>
        </button>
    </div>`;
      html += htmlSegment;
    });

    let category = document.querySelector('#category-list');
    category.innerHTML = html;
  }

  function deletedTask() {
    $('#deletedModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var id = button.attr('data-id');
      var url = 'api/task/delete/' + id;

      $(document).on('click', '#hapus-task', function () {
        $.ajax({
          url: url,
          type: 'DELETE',
          success: function (result) {
            $('#iniModal').modal('hide');
            window.location.reload();
          },
          error: function (err) {
            console.log(err);
          },
        });
      });
    });
  }

  function generateTaskForUpdate() {
    $('#itiModal ').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var id = button.attr('data-id');
      getTaskById(id);
      $(document).on('click', '#update-task', function () {
        validatForm('update', id);
      });
    });
  }

  function validatForm(page, id) {
    if (page == 'create') {
      var a = $('.category-name').val();
      var b = $('.title-task').val();
      var c = $('#deadline').val();
      var d = $('.total-point').val();
      var e = $('#asign-selected').val();
      let alert = document.getElementById('alert-create');

      titleCase(b);

      if (a == null || (b.length == 0 && c.length == 0) || c.length == 0 || d.length == 0 || e == null) {
        alert.style.display = 'block';
      } else {
        alert.style.display = 'none';
        createTask();
        window.location.reload();
      }
    } else {
      var a = $('.update-category').val();
      var b = $('.update-title').val();
      var c = $('.deadline').val();
      var d = $('.input-point').val();
      var e = $('#update-asign').val();
      let alert = document.getElementById('alert-update');

      titleCase(b);

      if (a == null || (b.length == 0 && c.length == 0) || c.length == 0 || d.length == 0 || e == null) {
        console.log('warning');
        alert.style.display = 'block';
      } else {
        alert.style.display = 'none';
        updateTask(id);
      }
    }
  }

  async function createTask() {
    const response = await fetch(`${LINK}/api/task/create`, {
      method: 'POST',
      // baseURL: "",
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        categoryName: $('.category-name').val(),
        title: inputTitle,
        deadline: $('#deadline').val(),
        point: $('.total-point').val(),
        asignFor: $('#asign-selected').val(),
      }),
    }).then((response) => {
      return 0;
    });
  }

  function createTaskSubmit() {
    $('#submit-task').on('click', function () {
      validatForm('create', 0);
    });
  }

  async function updateTask(id) {
    const response = await fetch(`${LINK}/api/task/update/` + id, {
      method: 'PUT',
      // baseURL: "",
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        categoryName: $('.update-category').val(),
        title: inputTitle,
        deadline: $('.deadline').val(),
        point: $('.input-point').val(),
        asignFor: $('#update-asign').val(),
      }),
    }).then((response) => {
      console.log(JSON.stringify(response));
      window.location.reload()
    });
  }

  function showCategorySelected(categoryName) {
    let dropdown = $('.category-name');
    dropdown.empty();

    dropdown.append('<option selected="true" disabled>Pilih kategori</option>');
    dropdown.prop('selectedIndex', 0);
    $.each(categoryName, function (key, category) {
      dropdown.append($('<option></option>').attr('value', category.name).text(category.name));
    });

    let categoryUpdateList = $('.update-category');
    categoryUpdateList.empty();

    categoryUpdateList.append('<option selected="true" disabled>Pilih kategori</option>');
    categoryUpdateList.prop('selectedIndex', 0);
    $.each(categoryName, function (key, category) {
      categoryUpdateList.append($('<option></option>').attr('value', category.name).text(category.name));
    });
  }

  function showChilds(res) {
    console.log(res.data);

    let asign1 = $('#asign-selected');
    asign1.empty();

    asign1.append('<option selected="true" disabled>Pilih anak</option>');
    asign1.prop('selectedIndex', 0);
    $.each(res.data, function (key, childs) {
      asign1.append($('<option></option>').attr('value', childs.username).text(childs.username));
    });

    let asign2 = $('#update-asign');
    asign2.empty();

    asign2.append('<option selected="true" disabled>Pilih anak</option>');
    asign2.prop('selectedIndex', 0);
    $.each(res.data, function (key, childs) {
      console.log(childs.username);
      asign2.append($('<option></option>').attr('value', childs.username).text(childs.username));
    });
  }

  function searchTask() {
    $('.search-task').on('keydown', function search(e) {
      if (e.keyCode == 13) {
        let input = $(this).val();
        titleCase(input);
        e.preventDefault();
        if (inputTitle.length != 0) {
          searchTaskByTitle(inputTitle);
        }
      }
    });
  }

  function searchTaskByTitle(input) {
    let url = 'api/task/search/' + input;
    $.ajax({
      url: url,
      type: 'GET',
      success: function (result) {
        console.log('search-data', result);
        showTaskData(result);
      },
      error: function (err) {
        console.log(err);
      },
    });
  }

  function titleCase(str) {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
      splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    inputTitle = splitStr.join(' ');
    console.log(inputTitle);
    return splitStr.join(' ');
  }

  function getDateTime(e) {
    searchTaskByDate(e.target.value);
  }

  function searchTaskByDate(input) {
    let date = new Date(input).toISOString();

    let url = 'api/task/search/date/' + date;
    $.ajax({
      url: url,
      type: 'GET',
      success: function (result) {
        console.log('search-byDate', result);
        showTaskData(result);
      },
      error: function (err) {
        console.log(err);
      },
    });
  }

  async function completedTask(id, poin) {
    var currentDate = new Date();
    var dd = String(currentDate.getDate()).padStart(2, '0');
    var mm = String(currentDate.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = currentDate.getFullYear();

    currentDate = yyyy + '-' + mm + '-' + dd;

    const response = await fetch(`${LINK}/api/task/update/` + id, {
      method: 'PUT',
      // baseURL: "",
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        completed: true,
        completedDate: currentDate,
      }),
    }).then((response) => {
      updatePoinUser(poin);
    });
  }

  async function updatePoinUser(poinReward) {
    let idUser = userData.id;
    let poinUser = userData.point;
    let totalPoin = poinUser + poinReward;

    const response = await fetch(`${LINK}/api/user/update/` + idUser, {
      method: 'PUT',
      // baseURL: "",
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        point: totalPoin,
      }),
    }).then((response) => {
      window.location.reload();
    });
  }

  function convertDate(inputDate) {
    const monthNames = ['-','Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
    let date = new Date(inputDate);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let dt = date.getDate();

    if (dt < 10) {
      dt = '0' + dt;
    }
    return dt + '-' + monthNames[month] + '-' + year;
  }
