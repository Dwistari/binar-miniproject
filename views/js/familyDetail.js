const url = `http://localhost:3000`

$(function () {
    getDataChilds()
    getDataTask()
})
   
async function getDataTask(id) {
    let url = `${url}/api/task/child` +id;
    try {
        const response = await fetch(url);
        var res = await response.json();
        console.log("getDataTask" + res + "->" + response)
        showTaskData(res);

    } catch (error) {
        console.log(error);
    }
}

function showTaskData(res) {
    let html = '';
    let htmlSegment = ""
    let viewTask = ""
    let onprogress = ""
    let doneTask = ""
    let deadline
    let completedDate

    if (res.status != 200) {
        html = `<div class="position-relative" style="min-height: calc(100vh - 350px)">
        <div class="position-absolute top-50 start-50 translate-middle text-center"><img
                src="/static/images/users/cuate.png" alt="">
            <p class="card-subtitle fc-text-arrow mt-2 fs-4 fw-bold">Wah belum ada task saat ini,<br />
                mulai
                tambahin yuk!</p>
        </div>
    </div>`
    } else {
        if (res.data == "undefined") {
            html = `<div class="position-relative" style="min-height: calc(100vh - 350px)">
            <div class="position-absolute top-50 start-50 translate-middle text-center"><img
                    src="/static/images/users/cuate.png" alt="">
                <p class="card-subtitle fc-text-arrow mt-2 fs-4 fw-bold">Wah belum ada task saat ini,<br />
                    mulai
                    tambahin yuk!</p>
            </div>
        </div>`
        } else {
            res.data.forEach((it, index) => {
                deadline = convertDate(it.deadline)
                completedDate = convertDate(it.completedDate)
                viewTask =
                    `<div class="row mb-3 bg-white task-item">
                                <div class="col-md-2 p-4 text-center" style="background-color:#5B8F9A">
                                    <div class="user text-center text-dark" style="font-family: 'DM Sans', sans-serif;">
                                        <img src="/static/images/users/Brother 2.png" class="rounded-circle" width="50">
                                        <p class="card-title mt-2 fw-normal fs-5 text-white">${it.asignFor}</p>
                                    </div>
                                </div>
                                <div class="col-md-7 p-3" style="height: 112px; width: 1109pxpx">
                                    <p class="m-0 p-0 fs-4 fw-bold">${it.title}</p>
                                    <p class="mt-2">${it.categoryName} &#x2022; ${it.point}pt,</p>
                                    <p class="mt-2"><i class="bi-alarm"></i> ${deadline}</p>
                                </div>
                              `
                onprogress =
                    `<div class="col-md-3 p-5">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <button type="button" class="btn bi bi-check-lg fs-5" onClick="completedTask(${it.id})" >
                                            </button>
                                        </div>
                                        <div class="col-md-4 ml-2">
                                            <!-- Button trigger modal Edit -->
                                            <button type="button" class="btn bi bi-pencil-fill fs-5" data-bs-toggle="modal"
                                            data-id=${it.id} data-bs-target="#itiModal">
                                            </button>
        
                                         
                                        </div>
                                        <div class="col-md-4 ml-2">
                                            <!-- Button trigger modal Trash -->
                                            <button type="button" class="btn bi bi-trash fs-5 delete-task" data-id="${it.id}" data-bs-toggle="modal"
                                                data-bs-target="#deletedModal" >
                                            </button>
                                        </div>
                                    </div>
                                    </div>
                                </div> `;

                doneTask = `<div class="col-md-3 p-4">
                <div class="row">
                    <b class="mt-2">Selesai</b>
                    <p class="mt-2"><i class="bi-alarm"></i> ${completedDate}</p>
                </div>
                </div>
            </div>`;
                if (it.completed == true) {
                    htmlSegment = viewTask + doneTask
                } else {
                    htmlSegment = viewTask + onprogress
                }

                html += htmlSegment;
            });
        }
    }

    let task = document.querySelector('.taskContainer');
    task.innerHTML = html;
}


function convertDate(inputDate) {
    const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
        "Juli", "Agustus", "September", "Oktober", "November", "Desember"
    ];
    let date = new Date(inputDate);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let dt = date.getDate();

    if (dt < 10) {
        dt = '0' + dt;
    }

    return dt + ' ' + monthNames[month] + ' ' + year
}

async function getDataChilds() {
  let url = `${url}/api/user/childs`;
  try {
    const response = await fetch(url);
    var res = await response.json();
    showChilds(res);
  } catch (error) {
    console.log(error);
  }
}

function showChilds(res) {
  let html = "";
  let htmlSegment = "";

  res.data.forEach((it, index) => {
    htmlSegment = `
        <p class="header-text ps-4">${it.username}</p>
        `;

    html += htmlSegment;
  });

  let task = document.querySelector("#nama-member");
  task.innerHTML = html;
}


