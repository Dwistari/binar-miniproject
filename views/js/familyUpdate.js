const url = `http://localhost:3000`

async function updateChild(id) {
  const response = await fetch(`${url}/api/user/update/${id}`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      username: $("#edit_username_child").val(),
      description: $("#edit_password_child").val(),
    }),
  });
  const data = await response.json();
  if (data.status == 200) {
    window.location.reload();
  }
}

$("#edit_button").click(function () {
  updateChild(id);
});

async function deleteChild(id) {
  const response = await fetch(
    `${url}/api/user/delete/${id}`,
    {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
  const data = await response.json();
  if (data.status == 200) {
    window.location.reload();
  }
}

$("#button_hapus").click(function () {
  deleteChild(id);
});
