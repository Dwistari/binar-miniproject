'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Reward extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Reward.belongsTo(models.user, { foreignKey: 'userId' });
      Reward.belongsTo(models.family, { foreignKey: 'familyId' });

    }
  };
  Reward.init({
    userId: DataTypes.INTEGER,
    familyId: DataTypes.INTEGER,
    namaReward: DataTypes.STRING,
    poin: DataTypes.INTEGER,
    claim: DataTypes.BOOLEAN,
    claimBy: DataTypes.STRING,
    claimDate: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Reward',
  });
  return Reward;
};