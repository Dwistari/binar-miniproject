'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Task extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Task.belongsTo(models.user, { foreignKey: 'userId' });
      Task.belongsTo(models.family, { foreignKey: 'familyId' });
      Task.belongsTo(models.Category, { foreignKey: 'categoryName' });
    }
  };
  Task.init({
    userId: DataTypes.BIGINT,
    familyId: DataTypes.INTEGER,
    categoryName: DataTypes.STRING,
    title: DataTypes.STRING,
    deadline: DataTypes.DATE,
    point: DataTypes.INTEGER,
    completed: DataTypes.BOOLEAN,
    completedDate: DataTypes.DATE,
    asignFor: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Task',
  });
  return Task;
};