'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    
    
    static associate(models) {
      // define association here
      user.belongsTo(models.role),
      user.hasMany(models.Task),
      user.belongsTo(models.family),
      user.hasMany(models.Note)
      user.hasMany(models.Reward);
    }
  };
  user.init({
    username: DataTypes.STRING,
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    roleId: DataTypes.INTEGER,
    avatar: DataTypes.STRING,
    familyId: DataTypes.INTEGER,
    point: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'user',
  });
  return user;
};