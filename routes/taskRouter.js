const router = require ("express").Router();

const controller = require ("../controller/taskController");

router.get('/',controller.getTaskPage);


module.exports = router;