const express = require('express');
const router = express.Router();
const authorize = require('../authorization/authorize');

const controller = require('../controller/controller');

var checkAuthentication = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  } else {
    res.redirect('/login');
  }
};

router.get('/', controller.getIndexPage);
router.get('/register', controller.getRegisterPage);
router.get('/login', controller.getLoginPage);
router.get('/memberlogin', controller.getMemberLoginPage);
router.get('/family', checkAuthentication, controller.getDashboardPage);

router.get('/editMember', controller.getEditMemberPage);
router.get('/detailMember', controller.getDetailMemberPage);

router.get('/logout', controller.logout);
router.get('/addmember', checkAuthentication, controller.getaddMemberPage);

router.post('/register', controller.register);
router.post('/login', controller.login);

router.post('/addmember', controller.addmember);
router.post('/memberlogin', controller.loginmember);
module.exports = router;
