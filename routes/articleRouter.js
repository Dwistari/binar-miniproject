const router = require ("express").Router();

const controller = require ("../controller/articleController");

router.get('/',controller.getArticlesPage);
router.post('/create',controller.create);
router.put("/:id", controller.update);
router.delete("/delete/:id", controller.delete);
router.get('/:slug', controller.getArticleDetailPage);

module.exports = router;
