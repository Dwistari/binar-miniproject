const router = require ("express").Router();

const controller = require ("../controller/apiController");

//Endpoin get user

router.get('/family',controller.getUserFamily)
router.get('/user/',controller.getUserData)
router.get('/user/childs',controller.getUserChild);
router.get('/user/detail/:name',controller.getUserChildDetail);
router.put('/user/update/:id',controller.updateUser);
router.delete('/user/delete/:id', controller.deleteChild);

//Endpoint notesx
router.get('/notes/list',controller.getNote);
router.get('/notes/:id',controller.getDetailNote);
router.post('/notes/create',controller.createNote);
router.put("/notes/update/:id", controller.updateNotes);
router.delete("/notes/deleted/:id", controller.deleteNotes);

//Endpoint Task
router.get('/task/list',controller.getTask);
router.get('/task/child/:id',controller.getTaskChild);
router.get('/task/:id',controller.getDetailTask);
router.post('/task/create',controller.createTask);
router.put("/task/update/:id", controller.updateTask);
router.delete("/task/delete/:id", controller.deleteTask);
router.get("/task/search/:title",controller.searchTask);
router.get("/task/search/date/:date",controller.searchTaskByDate);
router.get("/task/search/category/:category",controller.searchTaskByCategory);


// Endpoint Reward
router.get('/rewards/list', controller.getAllReward);
router.get('/rewards/list/claimed', controller.getRewardClaimed);
router.get('/rewards/:id', controller.getRewardById);
router.post('/rewards/create', controller.createReward);
router.put('/rewards/update/:id', controller.updateReward);
router.delete('/rewards/delete/:id', controller.deleteReward);

//Endpoit Category

router.get('/category/list',controller.getAllCategory);
router.get('/category/:id',controller.getCategory);
router.post('/category/create',controller.createCategory);
router.put("/category/:id", controller.updateCategory);
router.delete("/category/:id", controller.deleteCategory);


module.exports = router;