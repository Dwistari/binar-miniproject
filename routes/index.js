const router = require('express').Router();
const userRouter = require('../routes/router');
const taskRouter = require('../routes/taskRouter');
const noteRouter = require('../routes/noteRouter');
const rewardsRouter = require('../routes/rewardsRoutes');
const apiRouter = require('../routes/apiRouter');
const articleRouter = require('../routes/articleRouter');


var checkAuthentication = (req, res, next) => {
    if (req.isAuthenticated()) {
      return next();
    } else {
      res.redirect('/login');
    }
  };


router.use('/', userRouter);
router.use('/api', apiRouter);
router.use('/tasks', checkAuthentication, taskRouter);
router.use('/note', checkAuthentication, noteRouter);
router.use('/rewards', checkAuthentication, rewardsRouter);
router.use('/articles', checkAuthentication, articleRouter);

module.exports = router;
