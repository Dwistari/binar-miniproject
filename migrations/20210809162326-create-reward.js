'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Rewards', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'users',
          key: 'id',
        },
      },
      familyId: {
        type: Sequelize.INTEGER,
      },
      namaReward: {
        type: Sequelize.STRING
      },
      poin: {
        type: Sequelize.INTEGER
      },
      claim: {
        type: Sequelize.BOOLEAN
      },
      claimBy: {
        type: Sequelize.STRING
      },
      claimDate: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Rewards');
  }
};