
module.exports = {
  checkAuthentication : (req, res, next) => {
    
    const role = req.user.roleId
    
    if (role == 2) {
      res.redirect('/dashboardFamily');
    } else {
      return next();
    }
  }
}